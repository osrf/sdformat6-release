# REPOSITORY MOVED

## This repository has moved to

https://github.com/ignition-release/sdformat6-release

## Issues and pull requests are backed up at

https://osrf-migration.github.io/osrf-release-gh-pages/#!/osrf/sdformat6-release

## Until May 31st 2020, the mercurial repository can be found at

https://bitbucket.org/osrf-migrated/sdformat6-release
